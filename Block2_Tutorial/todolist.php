<?php
ini_set('display_errors', 'On');

define('ISITSAFETORUN', TRUE); 
require('mydatabase.php');

$dbhandle = mysqli_connect( $hostname, $username, $password ) 
    or die( "Unable to connect to MySQL");

$selected = mysqli_select_db($dbhandle, $mydatabase) 
    or die("Unable to connect to " . $mydatabase );

function validateLabel($label, &$errors) {
    if (!$label) {
        array_push($errors, 'Label must be populated.');
    }
    elseif (preg_match('/[<>]/',$label)) {
        array_push($errors, 'Label cannot contain < or >.');
    } 
}

function validateNotes($notes, &$errors) {
    if (!$notes) {
        array_push($errors, 'Notes must be populated.');
    }
    elseif (preg_match('/[<>]/', $notes)) {
        array_push($errors, 'Notes cannot contain < or >.');
    }
}

function getValidId(&$errors) {
    $value = intval($_POST['id']);
    if ($value <= 0) {
        array_push($errors, 'Invalid item ID.');
        return false;
    }
    return $value;
}



$actionMessages = array();
$actionErrors = array();
$inputLabel = '';
$inputNotes = '';
if (isset($_POST['action']) && $_POST['action'] == 'add') {
    $inputLabel = trim($_POST['label']);
    $inputNotes = trim($_POST['notes']);
    validateLabel($inputLabel, $actionErrors);
    validateNotes($inputNotes, $actionErrors);
    if (!$actionErrors) {
        // Add the message
        $sqlInsert = "insert into TODO_ITEM (LABEL, NOTES, DATE_ADDED)
                   values (?, ?, NOW())";
        $statement = mysqli_prepare($dbhandle, $sqlInsert);
        mysqli_stmt_bind_param(
            $statement,
            'ss',
            $inputLabel,
            $inputNotes);
        mysqli_stmt_execute($statement);
        mysqli_stmt_close($statement);
        array_push($actionMessages, 'The item has been added');
    }
}
elseif (isset($_POST['action']) && $_POST['action'] == 'delete') {
    $id = getValidId($actionErrors);
    if (!$actionErrors) {
        // Delete the task
        $sqlDelete = "delete from TODO_ITEM 
                      where ID = ?";
        $statement = mysqli_prepare($dbhandle, $sqlDelete);
        mysqli_stmt_bind_param(
            $statement,
            'i',
            $id);
        mysqli_stmt_execute($statement);
        mysqli_stmt_close($statement);
        array_push($actionMessages, 'The item has been deleted');
    }   
}
elseif (isset($_POST['action']) && $_POST['action'] == 'complete') {
    $id = getValidId($actionErrors);
    if (!$actionErrors) {
        // Mark the task completed
        $sqlComplete = "update TODO_ITEM 
                        set DATE_COMPLETED = NOW()
                        where ID = ?";
        $statement = mysqli_prepare($dbhandle, $sqlComplete);
        mysqli_stmt_bind_param(
            $statement,
            'i',
            $id);
        mysqli_stmt_execute($statement);
        mysqli_stmt_close($statement);
        array_push($actionMessages, 'The item has been marked completed');
    }   
}



?>

<!doctype html>
<html lang="en">
    <head>
        <title>Simple to-do list</title>
        <style type="text/css">
            body {
                font-family: sans-serif;
            }

            table {
                border-collapse: collapse;
            }
            
            table th, table td {
                border: 1px solid black;
                padding: 2px 4px 2px 4px;
            }

            .form-entry {
                margin-bottom:12px;
            }
            .form-label {
                float:left;
                width:100px;
            }

            .form-field {
                margin-left:100px;
            }

            .not-valid {
                display:block;
                color:red;
            }

            .valid {
                display:none;
            }

            #action-message {
                padding: 6px;
                border: 1px solid #808080;
                background-color: lightgreen;
            }

            #action-error {
                padding: 6px;
                border: 1px solid #808080;
                background-color: pink;
            }
        </style>


    </head>
    <body>
        <p><input type="checkbox" id="enable-validation" checked/><small>client-side validation enabled</small></p>
        <h1>Simple to-do list</h1>

        <?php 
        if ($actionMessages) {
            echo '<div id="action-message">';
            foreach($actionMessages as $m) {
                echo '<p>' . htmlspecialchars($m) . '</p>';
            }
            echo '</div>';
        }        
        if ($actionErrors) {
            echo '<div id="action-error">';
            foreach($actionErrors as $e) {
                echo '<p>' . htmlspecialchars($e) . '</p>';
            }
            echo '</div>';
        } 
        ?>

        <h2>Outstanding tasks</h2>
        <?php 
        // Show existing tasks

        $queryOutstandingSql = "select * 
                                from TODO_ITEM 
                                where DATE_COMPLETED is null
                                order by DATE_ADDED";
        $result = mysqli_query($dbhandle, $queryOutstandingSql) 
                  or die ("Could not action the query: $queryOutstandingSql");
        
        if (mysqli_num_rows($result) == 0) {
            echo '<p>There are no outstanding tasks</p>';
        }
        else {
            echo '<table>';
            echo '<tr><th>Date added</th><th>Label</th><th>Details</th></tr>';
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                ?>
                <tr>
                    <td><?php echo $row['DATE_ADDED']; ?>
                    <td><?php echo htmlspecialchars($row['LABEL']); ?></td>
                    <td><?php echo htmlspecialchars($row['NOTES']); ?></td>
                    <td>
                    <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>"
                          onsubmit="return validateDelete()">
                          <input type="hidden" name="action" value="delete"/>
                          <input type="hidden" name="id" value="<?php echo $row['ID']; ?>"/>
                          <button type="submit">Delete</button>
                    </form>
                    </td>
                    <td>
                    <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>"
                          onsubmit="return validateComplete()">
                          <input type="hidden" name="action" value="complete"/>
                          <input type="hidden" name="id" value="<?php echo $row['ID']; ?>"/>
                          <button type="submit">Mark complete</button>
                    </form>
                    </td>
                </tr>
                <?php
            }                      
            echo '</table>';
        }

        ?>

        <h2>Add a new task</h2>
        <div class="form">
            <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>"
                onsubmit="return validateAdd()">
                <input type="hidden" name="action" value="add"/>

                <div class="form-entry">
                    <div class="form-label">Label</div>
                    <div class="form-field">
                        <input type="text" id="add-label" name="label" value="<?php echo htmlspecialchars($inputLabel); ?>"/>
                     <br/>
                        <span id="add-label-message"></span>
                    </div>
                </div>
                <div class="form-entry">
                    <div class="form-label">Notes</div>
                    <div class="form-field">
                        <textarea id="add-notes" name="notes"><?php echo htmlspecialchars($inputNotes); ?></textarea>
                        <br/>
                        <span id="add-notes-message"></span>
                 </div>
                </div>
                <div class="form-entry">
                    <div class="form-field">
                        <button type="submit">Add</button>
                    </div>
                </div>
            </form>
        </div>

        <hr>
        <h2>Completed tasks</h2>

        
        <?php 
        // Show completed tasks
        $queryCompletedSql = "select * 
                                from TODO_ITEM 
                                where DATE_COMPLETED is not null
                                order by DATE_ADDED";
        $result = mysqli_query($dbhandle, $queryCompletedSql) 
                        or die ("Could not action the query: $queryCompletedSql");

        if (mysqli_num_rows($result) == 0) {
            echo '<p>There are no completed tasks</p>';
        }
        else {
            echo '<table>';
            echo '<tr><th>Date added</th><th>Date completed</th><th>Label</th><th>Details</th></tr>';
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                ?>
                <tr>
                    <td><?php echo $row['DATE_ADDED']; ?>
                    <td><?php echo $row['DATE_COMPLETED']; ?>
                    <td><?php echo htmlspecialchars($row['LABEL']); ?></td>
                    <td><?php echo htmlspecialchars($row['NOTES']); ?></td>
                    <td>
                    <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>"
                          onsubmit="return validateDelete()">
                          <input type="hidden" name="action" value="delete"/>
                          <input type="hidden" name="id" value="<?php echo $row['ID']; ?>"/>
                          <button type="submit">Delete</button>
                    </form>
                    </td>
                </tr>
                <?php
            }                      
            echo '</table>';
        }

        ?>

        <script type="text/javascript">
            function skipValidation() {
                var enableValidationCheck = document.getElementById('enable-validation');
                return !enableValidationCheck.checked;
            }

            function validateAdd() {
                if (skipValidation()) return true;

                let labelOk = validateLabel();
                let notesOk = validateNotes();
                return labelOk && notesOk;
            }

            function validateLabel() {
                let field = document.getElementById('add-label');
                let message = document.getElementById('add-label-message');
                let valid = false;
                if (field.value) {
                    if (/[<>]/.test(field.value)) {
                        message.innerText = 'The Label field cannot contain < or >';
                    }
                    else {
                        valid = true;
                        message.innerText = '';
                    }
                }
                else {
                    message.innerText = 'The Label field must be populated';
                }
                
                if (valid) {
                    message.classList.add('valid');
                    message.classList.remove('not-valid');
                }
                else {
                    message.classList.add('not-valid');
                    message.classList.remove('valid');
                }
                return valid;
            }

            function validateNotes() {
                let field = document.getElementById('add-notes');
                let message = document.getElementById('add-notes-message');
                let valid = false;
                if (field.value) {
                    if (/[<>]/.test(field.value)) {
                        message.innerText = 'The Notes field cannot contain < or >';
                    }
                    else {
                        valid = true;
                        message.innerText = '';
                    }
                }
                else {
                    message.innerText = 'The Notes field must be populated';
                }

                if (valid) {
                    message.classList.add('valid');
                    message.classList.remove('not-valid');
                }
                else {
                    message.classList.add('not-valid');
                    message.classList.remove('valid');
                }
                return valid;
            }

            function validateDelete() {
                if (skipValidation()) return true;
                return confirm('Are you sure you want to delete this item?');
            }

            function validateComplete() {
                if (skipValidation()) return true;
                return confirm('Are you sure you want to mark this item complete?');
            }
        </script>
    </body>
</html>

<?php
mysqli_close($dbhandle);
?>