<?php

    define('ISITSAFETORUN', TRUE); 

    require('mydatabase.php');

    $currentId = key_exists('id', $_GET) ? $_GET['id'] : '';

    $dbhandle = mysqli_connect( $hostname, $username, $password ) or die( "Unable to connect to MySQL");
    $selected = mysqli_select_db($dbhandle, $mydatabase) or die("Unable to connect to " . $mydatabase );
    $query = "SELECT * FROM PYPLdata ORDER BY share DESC";
    $result = mysqli_query($dbhandle, $query) 
        or die (" Could not action the query " . $query);
            

?>

<!doctype html>
<html lang="en">
    <head>
        <title>PHP demo</title>
        <style type="text/css">
            body {
            font-family: sans-serif;
            }

            table th, table td {
                border: 1px solid black;
                padding: 2px 4px 2px 4px;
            }
        </style>
    </head>
    <body>
    <h1>PHP demo - step 43, table created from database, with request parameter</h1>

        <table>
            <tr><th>Rank</th><th>Language</th><th>Share</th><th>Trend (yearly)</th></tr>
            <?php
            $rank = 1;
            $currentLogo = null;
            $currentLanguage = null;
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                echo '<tr>';
                echo "<td>$rank</td>";
                echo "<td><a href='?id={$row['id']}'>" . htmlspecialchars($row['language']) . '</a></td>';
                echo '<td>' . sprintf("%0.2f", $row['share']) . '%</td>';
                echo '<td>' . sprintf("%+0.1f", $row['trend']) . '%</td>';
                echo '</tr>';
                $rank++;
                
                if ($row['id'] == $currentId) {
                    $currentLanguage = $row['language'];
                    $currentLogo = $row['logo'];
                }
            }
            ?>
        </table>
        
        <?php
            if ($currentLogo != null) {
                echo "<p><img src='$currentLogo' alt='Logo for $currentLanguage'></p>";
            }
        ?>

        <p>Source: <a href="http://pypl.github.io/PYPL.html">PopularitY of Programming Language Index</a> (PYPL),
        which is based on Google searches for tutorials.</p>
    </body>
</html>

<?php
mysqli_close($dbhandle);
?>