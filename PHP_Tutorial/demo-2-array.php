<?php

define('ISITSAFETORUN', TRUE); 

$data = array(
    array('language' => 'Python', 'share' => 25.36, 'trend' => 5.2),
    array('language' => 'Java', 'share' => 21.56, 'trend' => -1.1),
    array('language' => 'JavaScript', 'share' => 8.4, 'trend' => 0.0),
    array('language' => 'C#', 'share' => 7.63, 'trend' => -0.4),
    array('language' => 'PHP', 'share' => 7.31, 'trend' => -1.3),
    array('language' => 'C/C++', 'share' => 6.4, 'trend' => -0.4)
);
?>

<!doctype html>
<html lang="en">
    <head>
        <title>PHP demo</title>
        <style type="text/css">
            body {
            font-family: sans-serif;
            }

            table th, table td {
                border: 1px solid black;
                padding: 2px 4px 2px 4px;
            }
        </style>
    </head>
    <body>
        <h1>PHP demo - step 2, table created from array of arrays</h1>

        <table>
            <tr>
                <th>Rank</th>
                <th>Language</th>
                <th>Share</th>
                <th>Trend (yearly)</th>
            </tr>
            <?php
            $rank = 1;
            foreach ($data as $row) {
                echo '<tr>';
                echo "<td>$rank</td>";
                echo "<td>{$row['language']}</td>";
                echo '<td>' . sprintf("%0.2f", $row['share']) . '%</td>';
                echo '<td>' . sprintf("%+0.1f", $row['trend']) . '%</td>';
                echo '</tr>';
                $rank++;
            }
            ?>
        </table>

        <p>Source: <a href="http://pypl.github.io/PYPL.html">PopularitY of Programming Language Index</a> (PYPL),
        which is based on Google searches for tutorials.</p>
    </body>
</html>