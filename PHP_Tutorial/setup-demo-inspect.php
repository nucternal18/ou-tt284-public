<!doctype html>
<html>
<head></head>
<body>
<?php

define('ISITSAFETORUN', TRUE); 

require 'mydatabase.php';

$dbhandle = mysqli_connect($hostname, $username, $password) or die( "Unable to connect to MySQL");

echo "<p>Connected to MySQL</p>";

$selected = mysqli_select_db($dbhandle, $mydatabase) or die("Unable to connect to $mydatabase");

echo "<p>Connected to MySQL database $mydatabase</p>";

$thisquery = "SHOW TABLES FROM $mydatabase";
$result = mysqli_query($dbhandle, $thisquery) or die (" Could not action the query $thisquery");
while ($row = mysqli_fetch_array($result)) {
	echo "<p>Table: {$row[0]}</p>"; 
}

mysqli_close($dbhandle);
?>
</html>