<?php

define('ISITSAFETORUN', TRUE); 

require('mydatabase.php');

$dbhandle = mysqli_connect( $hostname, $username, $password ) 
    or die( "Unable to connect to MySQL");

echo "<p>Connected to MySQL</p>";

//select a database to work with
$selected = mysqli_select_db(  $dbhandle, $mydatabase ) 
    or die("Unable to connect to " . $mydatabase );

echo "<p>Connected to MySQL database {$mydatabase}</p>";

$thisquery = "SHOW TABLES FROM " . $mydatabase ;// This is the SQL instruction
$result = mysqli_query( $dbhandle, $thisquery ) 
    or die (" Could not action the query " . $thisquery );
while ($row = mysqli_fetch_array($result)) {
	echo "<p>Table: {$row[0]}</p>"; //note that there is only one item in each row, so the first item is item zero
}
?>

<p>Now create our own table for testing. </p>

<?php   
// sql to create table
$sqlcreate = 
    "CREATE TABLE IF NOT EXISTS PYPLdata (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
language VARCHAR(30) NOT NULL,
share DECIMAL(5, 2) NOT NULL,
trend DECIMAL(5, 2) NOT NULL,
logo VARCHAR(255)
)";

if (mysqli_query($dbhandle, $sqlcreate)) {
    echo "<p>Table PYPLdata created successfully, or already exists</p>";
} else {
    echo "Error creating table: " . mysqli_error($dbhandle);
}

$sqldelete = "DELETE FROM PYPLdata";

if (mysqli_query($dbhandle, $sqldelete)) {
    echo "<p>Table PYPLdata cleared successfully</p>";
} else {
    echo "Error clearing table: " . mysqli_error($dbhandle);
}


$data = array(
    array('language' => 'Python', 'share' => 25.36, 'trend' => 5.2,
          'logo' => 'logo_python.png'),
    array('language' => 'Java', 'share' => 21.56, 'trend' => -1.1,
         'logo' => 'logo_java.jpeg'),
    array('language' => 'JavaScript', 'share' => 8.4, 'trend' => 0.0,
         'logo' => 'logo_javascript.png'),
    array('language' => 'C#', 'share' => 7.63, 'trend' => -0.4,
         'logo' => 'logo_csharp.png'),
    array('language' => 'PHP', 'share' => 7.31, 'trend' => -1.3,
         'logo' => 'logo_php.png'),
    array('language' => 'C/C++', 'share' => 6.4, 'trend' => -0.4,
         'logo' => 'logo_cpp.png')
);

$sqlinsert = "INSERT INTO PYPLdata (language, share, trend, logo) VALUES (?, ?, ?, ?)";
$statement = mysqli_prepare($dbhandle, $sqlinsert);

foreach($data as $row) {
    mysqli_stmt_bind_param(
        $statement,
        'sdds',
        $row['language'],
        $row['share'],
        $row['trend'],
        $row['logo']);
    mysqli_stmt_execute($statement);
}
mysqli_stmt_close($statement);
mysqli_close($dbhandle);
?>
